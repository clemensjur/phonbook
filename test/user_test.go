package test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/clemensjur/phon_book/controllers"
	"github.com/clemensjur/phon_book/db"
	"github.com/clemensjur/phon_book/models"
	"github.com/clemensjur/phon_book/routes"
	"github.com/clemensjur/phon_book/utils"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestRunUser(t *testing.T) {
	db.Db = db.CreateDb("pb_test")
	go t.Run("[START]StartServer", func(t *testing.T) {
		e := echo.New()
		routes.RouteApiV1(e)
		e.Logger.Fatal(e.Start(":8080"))
	})
	t.Run("[TEST]CreateUser", TestCreateUser)
	t.Run("[TEST]GetUsers", TestGetUsers)
	t.Run("[TEST]PickUser", TestPickUser)
	t.Run("[TEST]UpdateUser", TestUpdateUser)
	t.Run("[TEST]DeleteUser", TestDeleteUser)
}

func TestCreateUser(t *testing.T) {
	writer, payload := utils.CreateForm(map[string]string{
		"uuid":      "00000000-0000-0000-0000-000000000000",
		"username":  "user0",
		"password":  "pass0",
		"firstname": "first0",
		"lastname":  "last0",
		"email":     "mail0",
	})

	req := httptest.NewRequest(http.MethodPost, "/", payload)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	rec := httptest.NewRecorder()

	e := echo.New()
	c := e.NewContext(req, rec)
	c.SetPath("/users")

	var predictedJson = []byte(`{"uuid":"00000000-0000-0000-0000-000000000000","username":"user0","password":"pass0","firstname":"first0","lastname":"last0","email":"mail0"}`)
	var predictedUser models.User
	err := json.Unmarshal(predictedJson, &predictedUser)
	utils.CatchWithMessage(err, "Error parsing predicted user")

	if assert.NoError(t, controllers.CreateUser(c)) {
		var receivedUser models.User
		err = json.Unmarshal(rec.Body.Bytes(), &receivedUser)
		utils.CatchWithMessage(err, "Error parsing received user")

		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.Equal(t, predictedUser.Uuid, receivedUser.Uuid)
		assert.Equal(t, predictedUser.Username, receivedUser.Username)
		assert.Equal(t, predictedUser.Password, receivedUser.Password)
		assert.Equal(t, predictedUser.Firstname, receivedUser.Firstname)
		assert.Equal(t, predictedUser.Lastname, receivedUser.Lastname)
		assert.Equal(t, predictedUser.Email, receivedUser.Email)
	}
}

func TestGetUsers(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()

	e := echo.New()
	c := e.NewContext(req, rec)
	c.SetPath("/users")

	if assert.NoError(t, controllers.GetUsers(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestPickUser(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()

	e := echo.New()
	c := e.NewContext(req, rec)
	c.SetPath("/users/:uuid")
	c.SetParamNames("uuid")
	c.SetParamValues("00000000-0000-0000-0000-000000000000")

	var predictedJson = []byte(`{"uuid":"00000000-0000-0000-0000-000000000000","username":"user0","password":"pass0","firstname":"first0","lastname":"last0","email":"mail0"}`)
	var predictedUser models.User
	err := json.Unmarshal(predictedJson, &predictedUser)
	utils.CatchWithMessage(err, "Error parsing predicted user")

	if assert.NoError(t, controllers.PickUser(c)) {
		var receivedUser models.User
		err = json.Unmarshal(rec.Body.Bytes(), &receivedUser)
		utils.CatchWithMessage(err, "Error parsing received user")

		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, predictedUser.Username, receivedUser.Username)
		assert.Equal(t, predictedUser.Password, receivedUser.Password)
		assert.Equal(t, predictedUser.Firstname, receivedUser.Firstname)
		assert.Equal(t, predictedUser.Lastname, receivedUser.Lastname)
		assert.Equal(t, predictedUser.Email, receivedUser.Email)
	}
}

func TestUpdateUser(t *testing.T) {
	writer, payload := utils.CreateForm(map[string]string{
		"uuid":      "00000000-0000-0000-0000-000000000000",
		"username":  "user1",
		"password":  "pass1",
		"firstname": "first1",
		"lastname":  "last1",
		"email":     "mail1",
	})

	req := httptest.NewRequest(http.MethodPatch, "/", payload)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	rec := httptest.NewRecorder()

	e := echo.New()
	c := e.NewContext(req, rec)
	c.SetPath("/users/:uuid")
	c.SetParamNames("uuid")
	c.SetParamValues("00000000-0000-0000-0000-000000000000")

	var predictedJson = []byte(`{"uuid":"00000000-0000-0000-0000-000000000000","username":"user1","password":"pass1","firstname":"first1","lastname":"last1","email":"mail1"}`)
	var predictedUser models.User
	err := json.Unmarshal(predictedJson, &predictedUser)
	utils.CatchWithMessage(err, "Error parsing predicted user")

	if assert.NoError(t, controllers.UpdateUser(c)) {
		var receivedUser models.User
		err = json.Unmarshal(rec.Body.Bytes(), &receivedUser)
		utils.CatchWithMessage(err, "Error parsing received user")

		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, predictedUser.Username, receivedUser.Username)
		assert.Equal(t, predictedUser.Password, receivedUser.Password)
		assert.Equal(t, predictedUser.Firstname, receivedUser.Firstname)
		assert.Equal(t, predictedUser.Lastname, receivedUser.Lastname)
		assert.Equal(t, predictedUser.Email, receivedUser.Email)
	}
}

func TestDeleteUser(t *testing.T) {
	req, err := http.NewRequest(http.MethodDelete, "/", nil)
	utils.Catch(err)
	rec := httptest.NewRecorder()

	e := echo.New()
	c := e.NewContext(req, rec)
	c.SetPath("/users/:uuid")
	c.SetParamNames("uuid")
	c.SetParamValues("00000000-0000-0000-0000-000000000000")

	if assert.NoError(t, controllers.DeleteUser(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, "null\n", rec.Body.String())
	}
}
