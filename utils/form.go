package utils

import (
	"bytes"
	"mime/multipart"
)

func CreateForm(formMap map[string]string) (*multipart.Writer, *bytes.Buffer) {
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	for k, v := range formMap {
		_ = writer.WriteField(k, v)
	}

	err := writer.Close()
	Catch(err)

	return writer, payload
}
