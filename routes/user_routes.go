package routes

import (
	"github.com/clemensjur/phon_book/controllers"
	"github.com/labstack/echo/v4"
)

// RouteUsers
func RouteUsers(group *echo.Group) {

	// A POST request to /api/v1/users will create a new user with the specified form values
	group.POST("/users", controllers.CreateUser).Name = "create-user"

	// A GET request to /api/v1/users will return all user records found in the database
	group.GET("/users", controllers.GetUsers).Name = "get-users"

	// /api/v1/users/:uuid will return the record of the user with the given :uuid parameter
	group.GET("/users/:uuid", controllers.PickUser).Name = "pick-user"

	// A PATCH request to /api/v1/users/:uuid will update the specified user record of the database with the given form values
	group.PATCH("/users/:uuid", controllers.UpdateUser).Name = "update-user"

	// A DELETE request to /api/v1/users/:uuid will remove the entry from the database
	group.DELETE("/users/:uuid", controllers.DeleteUser).Name = "delete-user"
}
