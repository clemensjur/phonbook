package main

import (
	"github.com/clemensjur/phon_book/db"
	"github.com/clemensjur/phon_book/routes"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	db.Db = db.CreateDb("pb_dev")

	e := echo.New()
	e.Debug = true
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	routes.RouteApiV1(e)
	e.Static("/", "static/public")

	e.Logger.Fatal(e.Start(":8080"))
}
