package utils

func Catch(err error) {
	if err != nil {
		panic(err)
	}
}

func CatchWithMessage(err error, message string) {
	if err != nil {
		panic(message)
	}
}
