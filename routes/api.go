package routes

import (
	"github.com/labstack/echo/v4"
)

func RouteApiV1(e *echo.Echo) {
	api := e.Group("/api/v1")
	RouteUsers(api)
}
