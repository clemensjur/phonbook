package utils

import (
	"encoding/json"

	"gorm.io/gorm"
)

func SerializeJson(model *gorm.Model) string {
	json, err := json.Marshal(model)
	Catch(err)
	byteLen := len(json)
	return string(json[:byteLen])
}
