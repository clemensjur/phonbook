package db

import (
	"github.com/clemensjur/phon_book/models"
	"github.com/clemensjur/phon_book/utils"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var Db *gorm.DB

func CreateDb(dbname string) *gorm.DB {
	dsn := "root:@tcp(127.0.0.1:3306)/" + dbname + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	utils.Catch(err)
	db.AutoMigrate(&models.User{})
	return db
}
