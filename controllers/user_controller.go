package controllers

import (
	"net/http"

	"github.com/clemensjur/phon_book/db"
	"github.com/clemensjur/phon_book/models"
	"github.com/clemensjur/phon_book/utils"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

func CreateUser(c echo.Context) error {
	var user = models.User{
		Username:  c.FormValue("username"),
		Password:  c.FormValue("password"),
		Firstname: c.FormValue("firstname"),
		Lastname:  c.FormValue("lastname"),
		Email:     c.FormValue("email"),
	}
	if v, err := c.FormParams(); v.Has("uuid") {
		utils.Catch(err)
		user.Uuid = uuid.MustParse(v.Get("uuid"))
	} else {
		user.Uuid = uuid.New()
	}
	err := c.Bind(&user)
	utils.Catch(err)
	res := db.Db.Create(&user)
	log.Info(res)
	return c.JSON(http.StatusCreated, user)
}

func GetUsers(c echo.Context) error {
	var users []models.User
	db.Db.Find(&users)
	return c.JSON(http.StatusOK, users)
}

func PickUser(c echo.Context) error {
	var user models.User
	log.Info("UUID: " + c.Param("uuid"))
	db.Db.First(&user, "uuid = ?", c.Param("uuid"))
	return c.JSON(http.StatusOK, user)
}

func UpdateUser(c echo.Context) error {
	var user models.User
	err := c.Bind(&user)
	utils.Catch(err)
	log.Info("UUID: " + c.Param("uuid"))
	db.Db.Model(&user).Where("uuid = ?", c.Param("uuid")).Updates(map[string]interface{}{
		"username":  c.FormValue("username"),
		"password":  c.FormValue("password"),
		"firstname": c.FormValue("firstname"),
		"lastname":  c.FormValue("lastname"),
		"email":     c.FormValue("email"),
	})
	return c.JSON(http.StatusOK, user)
}

func DeleteUser(c echo.Context) error {
	var user models.User
	db.Db.Unscoped().Delete(&user, "uuid = ?", c.Param("uuid"))
	return c.JSON(http.StatusOK, nil)
}
